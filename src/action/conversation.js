export const addNewMessage = (newMessage) => ({
  type: 'SAVE_NEW_MESSAGE',
  payload: newMessage
})

export const saveConversationList = (conversationList) => ({
  type: 'SAVE_CONVERSATION_LIST',
  payload: conversationList
})

export const loadChat = (message, user, avatar, convID) => ({
  type: 'LOAD_CHATPAGE',
  payload: message,
  user: user,
  avatar: avatar,
  convID: convID
})
