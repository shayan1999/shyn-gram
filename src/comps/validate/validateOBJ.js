var validateOBJ = {
  firstname: {
    presence: {
      message: '^fill the field'
    },
    length: {
      minimum: 3,
      message: '^longer!'
    }
  },
  lastname: {
    presence: {
      message: '^fill the field'
    },
    length: {
      minimum: 3,
      message: '^longer!'
    }
  },
  email: {
    presence: {
      message: '^fill the field'
    },
    email: {
      message: "its's not an email"
    }
  },
  password: {
    presence: {
      message: '^fill the field.'
    },
    length: {
      minimum: 4,
      message: '^longer!'
    }
  },
  username: {
    presence: {
      message: '^fill the field'
    },
    length: {
      minimum: 3,
      message: '^longer!'
    }
  }
}
export default validateOBJ
