import React from 'react'
import Person from '../images/person.png'
import axios from 'axios'

class ProfMAker extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      selectedFile: null,
      description: null,
      phoneNumber: null,
      name: null,
      website: null,
      address: null
    }
  }
  fileSlectedHandler (event) {
    this.setState({
      selectedFile: event.target.files[0]
    })
  }
  changer (e) {
    var name = e.target.name
    this.setState({ [name]: e.target.value })
  }

  changeProf () {
    let fdata = new FormData()
    fdata.append('token', window.localStorage.getItem('token'))
    fdata.append('user_type', 'organization')
    fdata.append('description', this.state.description)
    fdata.append('phone_number', this.state.phoneNumber)
    fdata.append('avatar', this.state.selectedFile)
    fdata.append('location_lat', '43')
    fdata.append('location_long', '-79')
    fdata.append('mobile_number', this.state.phoneNumber)
    fdata.append('name', this.state.name)
    fdata.append('website', this.state.website)
    fdata.append('country_code', 'IR')
    fdata.append('address', this.state.address)
    axios.post('https://api.paywith.click/auth/profile/', fdata)
      .then(function (response) {
        console.log('response::::', response)
      })
      .catch(function (error) {
        console.log('error::::', error)
      })
  }
  render () {
    return (
      <div className='App'>
        <div className='BackStyle'>
          <div className='RightSide'>
            <div className='UpSide'>
              <div class='inputWithIcon'>
                <input
                  id='name' type='text' name='name' placeholder='Name' onChange={(e) => this.changer(e)}
                />
                <i
                  class='fas fa-user-tie'
                  aria-hidden='true' />
              </div>
              <div class='inputWithIcon'>
                <input
                  id='address'
                  type='text'
                  name='address'
                  placeholder='address'
                  onChange={(e) => this.changer(e)}
                />
                <i
                  class='fas fa-user-tag'
                  aria-hidden='true' />
              </div>
              <div class='inputWithIcon'>
                <input
                  id='website'
                  type='website'
                  name='website'
                  placeholder='website'
                  onChange={(e) => this.changer(e)}
                />
                <i
                  class='fas fa-at'
                  aria-hidden='true' />
              </div>
              <div class='inputWithIcon'>
                <input
                  id='bio'
                  type='text'
                  name='description'
                  placeholder='biography'
                  onChange={(e) => this.changer(e)}
                />
                <i
                  class='fas fa-book-open'
                  aria-hidden='true' />
              </div>
            </div>
            <div className='DownSide'>
              <h2 style={{ position: 'absolute', color: 'white', fontSize: '1.5vw' }} >
                this application has no resposibility to your informations. <br />
                don't expose your account to unknown people!!
              </h2>
              <input
                id='agreement'
                type='checkbox'
                name='agreement'
                value='true'
                style={{ marginTop: '25%', width: '5%', height: '5%' }}
              />
              <span style={{ color: 'white' }}>accept the agreements</span>
              <br />
              <div class='inputWithIcon' style={{ marginTop: '-10px', width: '40%' }}>
                <input
                  id='phone'
                  type='phoneNumber'
                  name='phoneNumber'
                  placeholder='phoneNumber'
                  style={{ height: '10%' }}
                  onChange={(e) => this.changer(e)}
                />
                <i
                  class='fas fa-phone'
                  aria-hidden='true'
                  style={{ top: '25px', right: '0px' }} />
              </div>
              <button
                className='redsubmit'
                style={{ marginTop: '5%' }}
                onClick={() => this.changeProf()}
              >
                Let's Go!!
              </button>
            </div>
          </div>
          <div className='LeftSide'>
            <div className='UpSide1'>
              <h1
                className='test'
                style={{ marginTop: '5%', fontSize: '2.8vw', left: '11%' }}
              >hello my friend!
              </h1>
              <h3
                className='test'
                style={{ marginTop: '15%', fontSize: '1.4vw', left: '13%' }}
              >please complete the form.
              </h3>
            </div>
            <div className='DownSide1'>
              <img className='userPic' src={Person} />
              <input
                type='file'
                placeholder='choos Photo!!'
                onChange={(event) => this.fileSlectedHandler(event) }
              />
            </div>
          </div>
        </div>
      </div>
    )
  }
}
export default ProfMAker
