import React from 'react'
import validate from '../validate/validateFUNC'
import { Link } from 'react-router-dom'
import axios from 'axios'

class SignUp extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      repassword: '',
      check: false,
      error: {
        userName: null,
        passWord: null,
        rePassWrod: null
      }
    }
  }
  handleChange (e) {
    console.log('e=', e.target.value) // e is event
    var name = e.target.name
    this.setState({ [name]: e.target.value })
  }
  handleclick () {
    var userNameError = validate('username', this.state.username)
    console.log(userNameError)
    var passWrodError = validate('password', this.state.password)
    console.log(passWrodError)
    var rePassWrodError = validate('password', this.state.repassword)
    console.log(rePassWrodError)
    this.setState({ ...this.state, error: { ...this.state.error, userName: userNameError, passWord: passWrodError, rePassWrod: rePassWrodError } })
    if (this.state.error.userName === null && this.state.error.passWord === null && this.state.error.rePassWrod === null) {
      if (this.state.password === this.state.repassword) {
        this.setState({ check: true })
        let data = {
          email: this.state.username,
          password: this.state.password
        }
        var a = false
        axios.post('https://api.paywith.click/auth/signup/', data)
          .then(function (response) {
            console.log('response::::', response)
            window.localStorage.setItem('token', response.data.token)
            window.localStorage.setItem('id', response.data.id)
            a = true
          })
          .catch(function (error) {
            console.log('error::::', error)
          })
        if (a) { this.setState({ check: true }) }
      } else {
        this.setState({ ...this.state, error: { ...this.state.error, rePassWrod: 'password and repassword not match' } })
      }
    }
  }
  render () {
    return (
      <div className='App'>
        <div className='BackStyle'>
          <div className='BackStyleSign1' />
          <div className='BackStyleSign2' />
          <div className='Container'>
            <h1 className='HStyle'>Sign Up</h1>
            <div className='BottomBorder' />
            <div class='inputWithIcon'>
              <input
                id='userName'
                type='text'
                name='username'
                placeholder='email'
                onChange={(e) => this.handleChange(e)}
              />
              <i class='fas fa-at' aria-hidden='true' />
            </div>
            <div class='inputWithIcon'>
              <input
                id='passWord'
                type='passWord'
                name='password'
                placeholder='passWord'
                onChange={(e) => this.handleChange(e)}
              />
              <i class='fas fa-unlock' aria-hidden='true' />
            </div>
            <div class='inputWithIcon'>
              <input
                id='confirmPass'
                type='passWord'
                name='repassword'
                placeholder='confirmPass'
                onChange={(e) => this.handleChange(e)}
              />
              <i class='fas fa-check-square' aria-hidden='true' />
            </div>
            <button
              className='submit'
              onClick={() => this.handleclick()}
            >
              SignUp!!
            </button>
            {this.state.check &&
            <Link
              className='submitLink'
              to='./ProfMaker'
              style={{ marginTop: '4%' }}
            >
                  I'm Ready! :D
            </Link>
            }
          </div>
        </div>
      </div>
    )
  }
}

export default SignUp
