import React from 'react'
import validate from '../validate/validateFUNC'
import axios from 'axios'
class Login extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      username: null,
      password: null,
      error: {
        userName: null,
        passWord: null
      }
    }
  }

  handleChange (e) {
    console.log('e=', e.target.value) // e is event
    var name = e.target.name
    console.log('name', e.target.name) // e is event
    this.setState({ [name]: e.target.value })
  }
  handleclick () {
    var userNameError = validate('username', this.state.username)
    console.log(userNameError)
    var passWrodError = validate('password', this.state.password)
    console.log(passWrodError)
    this.setState({ ...this.state, error: { ...this.state.error, userName: userNameError, passWord: passWrodError } })
    // var headers = {
    //   'Content-Type': 'application/json'
    // }
    if (this.state.error.userName === null && this.state.error.passWord === null) {
      axios.post('https://api.paywith.click/auth/signin/', { email: this.state.username, password: this.state.password })
        .then(function (response) {
          console.log('response::::', response)
          window.localStorage.setItem('token', response.data.data.token)
          window.localStorage.setItem('id', response.data.data.profile.id)
        })
        .catch(function (error) {
          console.log('error::::', error)
        })
    }
  }
  render () {
    if (this.state.username === '') { this.setState({ username: null }) }
    if (this.state.password === '') { this.setState({ password: null }) }
    return (
      <div className='App'>
        <div className='BackStyle'>
          <div className='BackStyleLog1' />
          <div className='BackStyleLog2' />
          <div className='Container'>
            <h1 className='HStyle'>Log In</h1>
            <div className='BottomBorder' />
            <div class='inputWithIcon'>
              <input
                id='userName'
                type='text'
                name='username'
                placeholder='email'
                onChange={(e) => this.handleChange(e)}
              />
              <i class='fas fa-at' aria-hidden='true' />
              { this.state.error.userName !== null &&
                <p
                  className='errorType'
                  style={{ bottom: '20px', right: '-35px' }}
                >
                  {this.state.error.userName}
                </p>
              }
            </div>
            <div class='inputWithIcon'>
              <input
                id='passWord'
                type='password'
                name='password'
                placeholder='password'
                onChange={(e) => this.handleChange(e)}
              />
              <i
                class='fas fa-user-lock'
                aria-hidden='true' />
              { this.state.error.passWord !== null &&
                <p
                  className='errorType'
                  style={{ bottom: '20px', right: '-35px' }}
                >
                  {this.state.error.passWord}
                </p>
              }
            </div>
            <button
              className='submit'
              onClick={() => this.handleclick()}
            >
                    Go
            </button>
            <br />
            <br />
            <a
              className='App-link'
              href='https://reactjs.org'
              target='_blank'
              rel='noopener noreferrer'
            >
                    forget password?
            </a>
          </div>
        </div>
      </div>
    )
  }
}

export default Login
