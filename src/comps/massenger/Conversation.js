import React from 'react'
import axios from 'axios'
import myImg from '../images/person.png'
import { loadChat } from '../../action/conversation'
export default class Conversation extends React.Component {
  clicked (token, id, size) {
    let date = (Math.floor(new Date().getTime() / 1000))
    let fdata = new FormData()
    fdata.append('token', token)
    fdata.append('conversation_id', id)
    fdata.append('size', size)
    fdata.append('date', date)
    axios.post('https://api.paywith.click/conversation/details/', fdata)
      .then((Response) => {
        console.log('response::', Response)
        this.props.dispatch(loadChat(Response.data.data.messages, this.props.name, this.props.avatar, id))
      })
      .catch((error) => {
        console.log('error', error)
      })
  }
  render () {
    return (
      <div className='conv' onClick={() => this.clicked(window.localStorage.getItem('token'), this.props.id, 50)}>
        <div className='profileContainer'>
          {this.props.Unseen !== 0 &&
            <span className='unSeen'>{this.props.Unseen}</span>
          }
          {this.props.Unseen === 0 &&
            <span className='unSeen0' />
          }
          {this.props.avatar !== 'https://api.paywith.click//media/default.jpg' &&
            <img src={this.props.avatar} style={{ width: '75px', height: '75px', marginLeft: '-30%', borderRadius: '75px' }} />
          }
          {this.props.avatar === 'https://api.paywith.click//media/default.jpg' &&
            <img src={myImg} style={{ width: '75px', height: '75px', marginLeft: '-30%', borderRadius: '75px' }} />
          }
        </div>
        <div className='contentContainer'>
          <div className='inf'>
            <span style={{ color: 'white', fontSize: '150%' }}>{this.props.name}</span>
            <span style={{ color: 'white' }}>{this.props.date.slice(5, 10)}</span>
          </div>
          <div style={{ textAlign: 'left' }}>
            <span style={{ color: 'black' }}>{this.props.latestMessage}</span>
          </div>
        </div>
      </div>
    )
  }
}
