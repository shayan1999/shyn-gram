import React from 'react'

export default class ChatScreen extends React.Component {
  constructor () {
    super()
    this.state = {
      messages: []
    }
  }

  // componentWillReceiveProps(nextProps) {
  //   console.log('nextpropsss', nextProps)
  //   if(this.props.messages !== nextProps.messages) {
  //     this.setState({haminjoori: true})
  //   }
  // }
  render () {
    // console.log('chat propssss', this.props.messages.sender.id)
    return (
      <div style={{ overflow: 'auto' }}>
        <div className='screen'>
          {this.props.messages.map((item, index) => {
            if (item.sender.id == window.localStorage.getItem('id')) {
              return (
                <div className='sender' key={index}>
                  <span className='message' style={{ borderRadius: '15px 15px 0 15px', backgroundColor: 'rgb(231, 72, 72)' }}>{item.text}</span>
                </div>
              )
            } else {
              return (
                <div className='receiver' style={{margin: '3px'}}>
                  <span className='message'style={{ marginBottom: '2px' }} >{item.text}</span>
                </div>
              )
            }
          })
          }
        </div>
      </div>
    )
  }
}
