import React from 'react'
import axios from 'axios'
import { saveConversationList } from '../../action/conversation'
import ConversationContainer from '../../container/ConversationContainer'

export default class ConversationList extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      conversationList: [],
      myId: window.localStorage.getItem('id'),
      token: window.localStorage.getItem('token'),
      suggestedUsers: [],
      search: false,
      showsearch: false
    }
    this.handleRequest = this.handleRequest.bind(this)
  }

  componentDidMount () {
    this.handleRequest()
  }

  handleRequest () {
    const token = window.localStorage.getItem('token')
    axios.get('https://api.paywith.click/conversation/', {
      params: {
        token: token
      }
    })
      .then(response => {
        this.props.dispatch(saveConversationList(response.data.data.conversation_details))
      })
      .catch(error => {
        console.log('1111111111', error)
      })
  }
  handleChange (e) {
    if (e.target.value === '') {
      this.setState({ showsearch: false })
    } else {
      this.setState({ showsearch: true })
    }
    let fdata = new FormData()
    fdata.append('token', this.state.token)
    fdata.append('query', e.target.value)
    fdata.append('size', 4)
    console.log('fdatta', fdata)
    axios.post('https://api.paywith.click/explore/search/contacts/', fdata)
      .then((response) => {
        console.log('response::::', response)
        this.setState({ suggestedUsers: response.data.data.users })
      })
      .catch((error) => {
        console.log('error::::', error)
      })
  }
  sugClick (token, id) {
    let fdata = new FormData()
    fdata.append('token', token)
    fdata.append('user_id', id)
    console.log('fdata', fdata)
    axios.post('https://api.paywith.click/conversation/', fdata)
      .then((response) => {
        console.log('response=', response)
      })
      .catch((error) => {
        console.log('error', error)
      })
  }
  searchon () {
    this.setState({ search: !this.state.search })
  }

  render () {
    return (
      <div className='ConversationList'>
        <i class='fas fa-search' style={{ position: 'absolute', top: '1vh', left: '1vh', fontSize: '5vh', cursor: 'pointer' }} onClick={() => this.searchon()} />
        {this.state.search &&
          <div style={{ position: 'absolute', backgroundColor: 'black', width: '100vw', height: '100vh', opacity: '0.8' }}>
            <div className='seachPage'>
              <div>
                <div class='inputWithIcon'>
                  <input
                    id='search'
                    type='text'
                    name='search'
                    placeholder='search pls!!!!'
                    onChange={(e) => this.handleChange(e)}
                  />
                  <i class='fas fa-search' aria-hidden='true' style={{ right: '5%' }} />
                </div>
                { this.state.suggestedUsers.map((user, index) => {
                  if (this.state.showsearch) {
                    return (
                      <p className='suggest' onClick={() => this.sugClick(window.localStorage.getItem('token'), user.id)}><img src={user.avatar_url} style={{ width: '2vw', height: '2vw', borderRadius: '2vw' }} /> {user.name}</p>
                    )
                  }
                })
                }
              </div>
            </div>
          </div>
        }
        { this.props.conversationList.map((conversation, index) => {
          return (
            conversation.users.map((user, idx) => {
              if (user.id != this.state.myId) {
                return (
                  <ConversationContainer
                    key={index}
                    name={user.email}
                    date={conversation.latest_message_date}
                    latestMessage={conversation.latest_message}
                    avatar={user.avatar_url}
                    id={conversation.id}
                  />
                )
              }
            })
          )
        })
        }
      </div>
    )
  }
}
