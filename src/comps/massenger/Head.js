import React from 'react'
import myImg from '../images/person.png'

export default class Head extends React.Component {
  constructor () {
    super()
    this.state = {
      user: '',
      avatar: ''
    }
  }
  render () {
    return (
      <div className='header'>
        {this.props.avatar !== '' &&
          <div className='headtohead'>
            <img src={this.props.avatar} style={{ width: '60px', margin: '0 5px', borderRadius: '60px' }} />
            <span style={{ color: 'white' }}> {this.props.user.slice(0, 5)} </span>
          </div>
        }
        {this.props.avatar === '' &&
          <div className='headtohead'>
            <img src={myImg} style={{ width: '60px', margin: '0 5px', borderRadius: '60px' }} />
            <span style={{ color: 'white', fontSize: '1.5vw' }}> select someone!!! </span>
          </div>
        }
      </div>
    )
  }
}
