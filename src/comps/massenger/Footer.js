import React from 'react'
import axios from 'axios'
import { addNewMessage } from '../../action/conversation'

export default class Chat extends React.Component {
  constructor () {
    super()
    this.state = {
      newMessage: '',
      convID: ''
    }
  }

  onChangeText (e) {
    this.setState({ newMessage: e.target.value })
    this.props.getNewMessage(e.target.value)
  }

  sendNewMessage (token, id) {
    this.props.dispatch(addNewMessage(this.state.newMessage))
    let fdata = new FormData()
    fdata.append('token', token)
    fdata.append('conversation_id', id)
    fdata.append('text', this.state.newMessage)
    axios.post('https://api.paywith.click/conversation/create/', fdata)
      .then((Response) => {
        console.log('pm:', Response)
      })
      .catch((error) => {
        console.log('pmError:', error)
      })
  }
  render () {
    console.log('props', this.props)
    return (
      <div className='footer'>
        <i class='far fa-grin-tongue-wink' style={{ flexGrow: '1', fontSize: '200%', color: 'white', marginRight: '-1%', marginLeft: '1%', cursor: 'pointer' }} onClick={() => this.emoji()} />
        <input className='pm' placeholder='write a message...' value={this.state.newMessage} onChange={(e) => this.onChangeText(e)} />
        <i class='fas fa-share' style={{ width: '20px', flexGrow: '1', marginLeft: '2%', color: 'white', cursor: 'pointer', fontSize: '150%' }} onClick={() => this.sendNewMessage(window.localStorage.getItem('token'), this.props.convID)} />
      </div>
    )
  }
}
