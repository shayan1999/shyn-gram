import React from 'react'
// import person from '../images/person.png'
import EmojiPicker from 'emoji-picker-react'
import ChatScreenContainer from '../../container/ChatScreenContainer'
import FooterContainer from '../../container/FooterContainer'
import HeadContainer from '../../container/HeadContainer'
import Head from './Head'

export default class Chat extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      emoji: false,
      pickedEmoji: null,
      newMessage: ''
    }
  }
  emoji () {
    this.setState({ emoji: !this.state.emoji })
  }
  getNewMessage (newMessage) {
    this.setState({ newMessage })
  }
  render () {
    return (
      <div className='chatPage'>
        <HeadContainer />
        <ChatScreenContainer newMessage={this.state.newMessage} />
        {this.state.emoji === true &&
          <div style={{ position: 'absolute', bottom: '10vh' }}>
            <EmojiPicker onEmojiClick={this.state.pickedEmoji} />
          </div>
        }
        <FooterContainer getNewMessage={(newMessage) => this.getNewMessage(newMessage)} />
      </div>
    )
  }
}
