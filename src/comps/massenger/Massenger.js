import React from 'react'
import ConversationListContainer from '../../container/ConversationList Container'
import Chat from './Chat'

export default class Massenger extends React.Component {
  render () {
    return (
      <div style={{ width: '100vw', display: 'flex', flexDirection: 'column', height: '100vh', backgroundColor: 'grayvvdcc' }}>
        <div className='nameview'>
          <p>thanks for using SHYNGram!!!</p>
        </div>
        <div style={{ display: 'flex' }}>
          <ConversationListContainer />
          <Chat />
        </div>
      </div>
    )
  }
}
