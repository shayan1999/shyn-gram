import { connect } from 'react-redux'
import ChatScreen from '../comps/massenger/ChatScreen'

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  }
}

const mapStateToProps = state => {
  return {
    newMessage: state.newMessage,
    messages: state.messages
  }
}

const ChatScreenContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ChatScreen)

export default ChatScreenContainer
