import { connect } from 'react-redux'
import HeadScreen from '../comps/massenger/Head'

const mapDispatchToProps = dispatch => {
  return {
    dispatch: dispatch
  }
}

const mapStateToProps = state => {
  return {
    user: state.user,
    avatar: state.avatar
  }
}

const HeadContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(HeadScreen)

export default HeadContainer
