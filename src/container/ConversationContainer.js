import { connect } from 'react-redux'
import Conversation from '../comps/massenger/Conversation'
const mapDispatchToProps = dispatch => ({
  dispatch: dispatch
})

const ConversationContainer = connect(
  mapDispatchToProps
)(Conversation)

export default ConversationContainer
