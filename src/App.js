import React from 'react'
import './App.css'
import Login from './comps/startings/Login'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import SignUp from './comps/startings/SignUp'
import ProfMAker from './comps/startings/ProfMaker'
import Massenger from './comps/massenger/Massenger'
import { Provider } from 'react-redux'
import { createStore } from 'redux'
import conversation from './reducer/conversation'

const store = createStore(conversation)

class App extends React.Component {
  render () {
    return (
      <Provider store={store}>
        <Router>
          <Route exact path='/' component={Login} />
          <Route path='/SignUp' component={SignUp} />
          <Route path='/ProfMaker' component={ProfMAker} />
          <Route path='/mes' component={Massenger} />
        </Router>
      </Provider>
    )
  }
}

export default App
