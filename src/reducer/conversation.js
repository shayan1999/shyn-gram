const INIT = {
  newMessage: '',
  messages: [],
  conversationList: [],
  user: '',
  avatar: ''
}

var myid = window.localStorage.getItem('id')
function conversation (state = INIT, action) {
  switch (action.type) {
    case 'SAVE_NEW_MESSAGE':
      return { ...state,
        newMessage: action.payload,
        messages: [
          ...state.messages,
          {
            sender: {
              id: myid
            },
            text: action.payload
          }
        ]
      }
    case 'SAVE_CONVERSATION_LIST':
      return {
        ...state,
        conversationList: action.payload
      }
    case 'LOAD_CHATPAGE':
      console.log('messagessss', action)
      return { ...state,
        messages: action.payload,
        user: action.user,
        avatar: action.avatar,
        convID: action.convID
      }
    default:
      return state
  }
}

export default conversation
